
# Single-cell RNA-seq revealed a concomitant delay in differentiation and cell cycle of aged hematopoietic stem cell

***Leonard Herault***

## Abstract

Hematopoietic stem cells (HSCs) are the guarantor of the proper functioning of hematopoiesis due to their incredible diversity of potential. During aging the heterogeneity of mouse HSCs evolves, which contributes to the deterioration of the immune system. Here we address the transcriptional plasticity of HSC upon aging at the single-cell resolution. Through the analysis of 15,000 young and aged transcriptomes, we reveal 15 clusters of HSCs unveiling rare and specific HSC potentials that change with age. Pseudotime ordering complemented with regulon analysis showed that the consecutive differentiation states of HSC are delayed upon aging. By analysing cell cycle at the single cell level we highlight an imbalance of cell cycle regulators of very immature aged HSC that may contribute to their accumulation in an undifferentiated state.
Our results therefore establish a reference map of young and aged mouse HSC differentiation and reveal a potential mechanism that delay aged HSC differentiation.






## Availabilty of data

The single-cell RNA-seq data generated in our study are available in the Gene Expression Omnibus database under accession code GSE147729.

## Analysis and script

We provide in this repository the snakemake workflow (Snakefile.py) and its configuration (config/snakemake_workflow.yml) we developped to analyse the single cell RNA seq data.
It is built mainly on R scripts with [Seurat v3](https://satijalab.org/seurat/) for the sample integration and the clustering of the cells and with [Monocle v2](http://cole-trapnell-lab.github.io/monocle-release/docs/) for the pseudotime ordering of the cells. It uses [pySCENIC](https://pyscenic.readthedocs.io/en/latest/) command line tools for the SCENIC part of the workflow.

See the materials and methods section of the corresponding manuscript for more details.
The final step of the workflow produce an html report with the figures shown in the publication.
Our html produced from this [Rmarkdown file](report/final_report.Rmd) can be download [here](report/our_final_report.html) and view in a html browser.


## Installation

This snakemake workflow works with conda on Linux.
You need first to download and install [conda with python version 3.7](https://docs.conda.io/en/latest/miniconda.html).
Then once you have downloaded the repository, you can create the snakemake environment with:

    conda env create -n snakemake -f config/snakemake_env.yml
    
    
The workflow starts from the output of the cellranger pipeline [v2.2](https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/2.2/what-is-cell-ranger) from 10X returning for each of our samples a filtered gene-barcode-matrix. 
Each matrix is stored in Market Exchange Format (MEX). It also contains TSV files with genes and barcode sequences corresponding to row and column indices, respectively.

You can download all these files in a tar archive on GEO under accession code GSE147729 (supplementary data section).
To run the workflow you need to create a folder input/GEO in the downloaded repository and place the tar archive inside.
You can then activate snakemake env and then launch the workflow.

### Activate snakemake environment

    conda activate snakemake

### Command line to launch the  workflow 
Being in the downloaded repository:

    snakemake -j 20 \
        -nps Snakefile.py \
        --configfile config/snakemake_workflow.yml \
        --use-conda


### command lines to launch the snakemake workflow on a cluster with slurm
First being in the downloaded repository, you have to create a directory where standard output and error will be written:
    
    mkidr -p cluster/snakemake/

Then launch the snakemake (you have to set the local variables to en_US.UTF-8 and add the absolute miniconda3/bin path in your $PATH if it is not the case):

    snakemake -j 7 \
        -nps Snakefile.py \
        --configfile config/snakemake_workflow.yml \
        --use-conda \
        --cluster-config config/cluster.yml \
        --cluster "sbatch -A {cluster.account} \
        -p {cluster.partition} \
        -N {cluster.N} \
        -t {cluster.time} \
        --job-name {cluster.name} \
        --cpus-per-task {cluster.cpus-per-task}\
        --output {cluster.output} \
        --error {cluster.error}"


