# coding: utf-8
#Trying hscScore model
# In[1]:


import pandas as pd
import numpy as np
import pickle
import sklearn
import platform

#data_dir = './Data_for_review/'
#matrix_dir = '/shared/projects/scRNA_HSPC_Aging/scRNA_infer/input/'


# In[2]:


def total_count_normalise(count_matrix):
    """Normalise count matrix for input into hscScore model.
    Performs read depth normalisation normalising each cell so that normalised 
    counts sum to the same value.
    
    Parameters
    ----------
    count_matrix : pandas dataframe
        Gene count matrix of dimension cells x genes with column names as genes
        and index as cell names
    
    Returns
    -------
    **norm_matrix** : pandas dataframe
        Normalised count matrix of dimension cells x genes
    """
    
    # Set the value normalised counts will sum to for each cell
    wilson_molo_genes_median_counts = 18704.5
    
    # Scale rows
    count_matrix_expression = np.array(count_matrix, dtype='float')
    counts_per_cell = np.sum(count_matrix_expression, axis=1)
    counts_per_cell += (counts_per_cell == 0)
    counts_per_cell /= wilson_molo_genes_median_counts
    norm_matrix_expression =  count_matrix_expression/counts_per_cell[:, None]
    norm_matrix = pd.DataFrame(norm_matrix_expression, index=count_matrix.index,
                               columns=count_matrix.columns)
    # log + 1 transform the data
    norm_matrix = np.log(norm_matrix + 1)
    
    return norm_matrix

#Load training model :
# In[3]:


hsc_score = pickle.load(open('publicData/hscScore/hscScore_model.pkl', 'rb'))


# In[4]:


count_data = pd.read_csv('output/hscScore/dataMatrix.csv', sep=',', header=0,index_col = 0).T
count_data.head()


# In[5]:


model_genes = np.genfromtxt('publicData/hscScore/model_molo_genes.txt', dtype='str')
model_genes


# In[6]:


count_data_molo = count_data[model_genes]
count_data_molo.head()


# In[7]:


normalised_data_molo = total_count_normalise(count_data_molo)
normalised_data_molo.head()


# In[8]:


predicted_hsc_scores = hsc_score.predict(np.array(normalised_data_molo) )
predicted_hsc_scores


# In[13]:


np.savetxt("output/hscScore/hscScore.csv", predicted_hsc_scores, delimiter=',')

