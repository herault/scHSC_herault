import os
import re

# Get samples and ages from config file
samples = config["samples"]
samples = samples.split(",")
print(samples)

ages = config["ages"]
ages = ages.split(",")
print(ages)

# Get conda env file from config file
seurat_monocle_env = config["seurat_monocle_env"]
pyscenic_env = config["pyscenic_env"]
report_env = config["report_env"]
hscScore_env = config["hscScore_env"]

feather_database = config["scenic"]["cis_target_url"].split("/")[-1]
print(feather_database)

# Define base directory
baseDir = config["dir"]["base"]
os.chdir(baseDir)

print(samples)


rule all:
    input: "report/final_report.html",
           "report/BMC_reviewing.docx",
           "report/final_report_firstReview.html"
rule untar:
    input: "input/GEO/" + config["GEO_tar"]
    output:expand("input/GEO/{smp0}_barcodes.tsv",smp0 = samples),
           expand("input/GEO/{smp0}_genes.tsv",smp0 = samples),
           expand("input/GEO/{smp0}_matrix.mtx",smp0 = samples)
    shell: "tar xvf {input} -C input/GEO;\
    cd input/GEO; for file in *.gz ; do mv $file `echo $file | sed 's/GSM[0-9]*_//'` ; done;gunzip *.gz"
    

rule load_10X_data_from_GEO_download:
    input: "input/GEO/{smp}_barcodes.tsv",
           "input/GEO/{smp}_genes.tsv",
           "input/GEO/{smp}_matrix.mtx"
    output: "input/10X_data_{smp}.rds"
    params: age = lambda wildcards : config[wildcards.smp]["age"],
            runDate = lambda wildcards : config[wildcards.smp]["date"],
            cellranger = config["cellrangerVersion"]
    threads: 1
    conda: seurat_monocle_env
    shell: "Rscript R_src/load_10X_data.R -c {params.cellranger} -b input/GEO -f {output} -n {wildcards.smp} -s age={params.age},runDate={params.runDate},sampleName={wildcards.smp}"


rule prepare_data_for_monocle_seurat:
    input: data = "input/10X_data_{smp}.rds"
    output: "output/{smp}/dataPreparation/gbm_cds_treated.rds"
    threads: 1
    conda: seurat_monocle_env
    shell: "Rscript R_src/prepareCL.R -i {input.data} -o output/{wildcards.smp}/dataPreparation"


rule download_rodriguez_data:
  params: config["Rodriguez_data"]["LTHSC"],
          config["Rodriguez_data"]["STHSC"],
          config["Rodriguez_data"]["MPP2"],
          config["Rodriguez_data"]["MPP3"]
  output: "publicData/Rodriguez/rawCount/download_done",
          "publicData/Rodriguez/rawCount/GSM2411664_LTHSC.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411668_STHSC.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411665_MPP2.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411666_MPP3.raw_umifm_counts.csv"
  shell: "cd publicData/Rodriguez/rawCount/;wget {params[0]} {params[1]} {params[2]} {params[3]};gunzip *;touch download_done"


rule download_signatures:
    params: geiger = config["publicSignatures"]["geiger_url"],
            chambers = config["publicSignatures"]["chambers_url"],
            rodriguez_clusters = config["publicSignatures"]["rodriguez_clusters_url"],
    output: geiger = "publicData/geiger_signatures.xls",
            chambers = "publicData/chambers_signatures.xls",
            rodriguez = "publicData/rodriguez_cluster_signatures.xlsx"
    shell: """wget -cO - {params.geiger} > {output.geiger};
    wget -cO - {params.chambers} > {output.chambers};
    wget -cO - {params.rodriguez_clusters} > {output.rodriguez}"""


rule get_signatures:
    input: geiger = "publicData/geiger_signatures.xls",
           chambers = "publicData/chambers_signatures.xls",
    output: "output/signatures/publicSignatures.rds"
    threads: 12
    conda: seurat_monocle_env
    shell : "Rscript R_src/getSignaturesCL.R -t {input.geiger} -m {input.chambers} -o output/signatures/"


rule prepare_rodriguez_data:
  input: "publicData/Rodriguez/rawCount/GSM2411664_LTHSC.raw_umifm_counts.csv",
         "publicData/Rodriguez/rawCount/GSM2411668_STHSC.raw_umifm_counts.csv",
         "publicData/Rodriguez/rawCount/GSM2411665_MPP2.raw_umifm_counts.csv",
         "publicData/Rodriguez/rawCount/GSM2411666_MPP3.raw_umifm_counts.csv"
  output: "output/RodriguezPreparation/seuratForCastle.rds"
  conda: seurat_monocle_env
  shell: "Rscript R_src/prepareRodriguezCL.R -i publicData/Rodriguez/rawCount/ -o output/RodriguezPreparation"
  
  
rule seurat3:
    input: dataPrepared =  "output/{smp}/dataPreparation/gbm_cds_treated.rds",
           sig = "output/signatures/publicSignatures.rds",
           rodriguez = "publicData/rodriguez_cluster_signatures.xlsx"
    output: "output/{smp}/Seurat3/seurat.rds"
    conda: seurat_monocle_env
    params: num_dim = config["seurat3"]["dim"],
        cr = config["seurat3"]["correction"],
        minPropCell = config["seurat3"]["minPropCellExp"],
        res = config["seurat3"]["resolution"]
    threads: 1
    shell: "Rscript R_src/Seurat3CL.R -i {input.dataPrepared} \
    -o output/{wildcards.smp}/Seurat3 -n {params.num_dim} \
    -b {params.cr} -s {input.sig} -k {input.rodriguez} \
    -r {params.res} -z logNorm"


rule castle:
   input: smp = "output/{smp}/Seurat3/seurat.rds",
          source = "output/RodriguezPreparation/seuratForCastle.rds"
   output: "output/{smp}/castle/seuratTrainedCt.rds"
   conda: seurat_monocle_env
   threads: 1
   shell: "Rscript R_src/castleSeurat3CL.R -i {input.source} -t {input.smp} -o output/{wildcards.smp}/castle/"


rule seurat3_integration:
    input: expand("output/{smp}/castle/seuratTrainedCt.rds",smp=samples)
    output: "output/Seurat3_integration/combined.rds"
    conda: seurat_monocle_env
    params: cr = config["seurat_integration"]["correction"],
            num_dim_smp = config["seurat_integration"]["dim"],
            num_dim_int = config["seurat_integration"]["num_dim_int"],
            smpList = config["seurat_integration"]["smp_list"],
            res = config["seurat_integration"]["resolution"]
    threads: 1
    shell: "Rscript R_src/Seurat3_integration.R -i {params.smpList} -n {params.num_dim_smp} -N {params.num_dim_int} -o output/Seurat3_integration -r {params.res} -b {params.cr} -z logNorm"


rule seurat3_integration_young:
    input: expand("output/{smp}/castle/seuratTrainedCt.rds",smp=samples)
    output: "output/young/Seurat3_integration/combined.rds"
    conda: seurat_monocle_env
    params: num_dim_smp = config["seurat_integration_young"]["dim"],
            num_dim_int =  config["seurat_integration_young"]["num_dim_int"],
            cr = config["seurat_integration_young"]["correction"],
            smpList = config["seurat_integration_young"]["smp_list"],
            res = config["seurat_integration_young"]["resolution"]
    threads: 1
    shell: "Rscript R_src/Seurat3_integration.R -i {params.smpList} \
    -n {params.num_dim_smp} -N {params.num_dim_int} -o output/young/Seurat3_integration \
    -r {params.res} -b {params.cr} -z logNorm"


rule seurat3_integration_old:
    input: expand("output/{smp}/castle/seuratTrainedCt.rds",smp=samples)
    output: "output/old/Seurat3_integration/combined.rds"
    conda: seurat_monocle_env
    params: num_dim_smp = config["seurat_integration_old"]["dim"],
            num_dim_int =  config["seurat_integration_old"]["num_dim_int"],
            cr = config["seurat_integration_old"]["correction"],
            smpList = config["seurat_integration_old"]["smp_list"],
            res = config["seurat_integration_old"]["resolution"]
    threads: 1
    shell: "Rscript R_src/Seurat3_integration.R -i {params.smpList} \
    -n {params.num_dim_smp} -N {params.num_dim_int} -o output/old/Seurat3_integration \
    -r {params.res} -b {params.cr} -z logNorm"


rule seurat3_biology:
    input: data = "output/Seurat3_integration/combined.rds",
           rodriguez = "publicData/rodriguez_cluster_signatures.xlsx",
           sig = "output/signatures/publicSignatures.rds"
    output: "output/Seurat3_integration/biology/markers_res0.5/clusters_table.tsv",
            "output/Seurat3_integration/biology/markers_res0.5/markers.tsv",
            "output/Seurat3_integration/biology/markers_res"+config["seurat_biology"]["resolution"]+"/markers_and_bonzanni_TF.tsv"
    conda: seurat_monocle_env
    params: res = config["seurat_biology"]["resolution"],
            logfc_thr =config["seurat_biology"]["logfc_threshold"]
    shell: "Rscript R_src/Seurat3_integration_biology.R -i {input.data} \
    -o  output/Seurat3_integration/biology/ -r {params.res} -s {input.sig} \
    -l {params.logfc_thr} -k {input.rodriguez} -z logNorm"



rule ordering_from_seurat3Integrated_results_all:
    input: seurat = "output/Seurat3_integration/combined.rds",
           sig = "output/signatures/publicSignatures.rds",
           sigClustTable = "output/Seurat3_integration/biology/markers_res0.5/clusters_table.tsv"
    output: "output/MonocleIntegrationWoPb/monocleWithCounts.rds"
    params: col = "integrated_snn_res.0.5"
    conda: seurat_monocle_env
    threads: 1
    shell:  "Rscript R_src/orderingSeurat3IntegratedMonocleCL.R -i {input.seurat} \
    -o output/MonocleIntegrationWoPb/ \
    -s {input.sig} -k {params.col} \
    -t {input.sigClustTable} -d preB"
    
    

rule ordering_from_seurat3Integrated_results:
    input: seurat = "output/{age}/Seurat3_integration/combined.rds",
           sig = "output/signatures/publicSignatures.rds"
    output: "output/{age}/MonocleIntegration/monocleWithCounts.rds"
    params: col = "integrated_snn_res.0.8"
    conda: seurat_monocle_env
    threads: 1
    shell:  "Rscript R_src/orderingSeurat3IntegratedMonocleCL.R -i {input.seurat}\
    -o output/{wildcards.age}/MonocleIntegration/ \
    -s {input.sig} -k {params.col}"


#### Scenic part of the workflow

rule get_databases_for_scenic:
    output: feather="publicData/database/mm9-tss-centered-10kb-7species.mc9nr.feather",
            tbl="publicData/database/motifs-v9-nr.mgi-m0.001-o0.0.tbl"
    params: feather=config["scenic"]["cis_target_url"],
            tbl=config["scenic"]["motif_url"],
            sums=config["scenic"]["sums"],
            feather_name=feather_database
    shell: """cd publicData/database; \
    wget {params.feather}; \
    wget {params.tbl}; \
    wget {params.sums}; \
    awk -v feather_database={params.feather_name} '$2 == feather_database' sha256sum.txt | sha256sum -c -"""



rule prepare_tf_list:
     input: "publicData/database/motifs-v9-nr.mgi-m0.001-o0.0.tbl"
     output: "output/publicData/mm_mgi_tfs.txt"
     conda: pyscenic_env
     shell: "python py_src/getTfList.py -i {input} -o {output}"


rule filter_genes_for_scenic_all:
    input: "output/Seurat3_integration/combined.rds",
           "output/Seurat3_integration/biology/markers_res"+config["seurat_biology"]["resolution"]+"/markers.tsv",
           "output/publicData/mm_mgi_tfs.txt"
    output: "output/ScenicAll/expressionRawCountFilteredForScenic.tsv",
            "output/ScenicAll/markers_and_bonzanni_TF.tsv"
    conda: seurat_monocle_env
    shell: "Rscript R_src/filterForScenicCL.R -i {input[0]} -m {input[1]} -t {input[2]} -o output/ScenicAll/"

    

rule grnboost_all:
    input: "output/ScenicAll/expressionRawCountFilteredForScenic.tsv",
           "output/ScenicAll/markers_and_bonzanni_TF.tsv"
    output: "output/ScenicAll/GRNboost/GRNboost.tsv"
    threads: 20
    conda: pyscenic_env
    shell:
        "pyscenic grn --seed 2020 --num_workers {threads} {input[0]} {input[1]} -o {output}"
        
        
 
rule prune_modules_with_cis_target_csv_all:
  input:
        exp="output/ScenicAll/expressionRawCountFilteredForScenic.tsv",
        GRN_boost="output/ScenicAll/GRNboost/GRNboost.tsv",
        TF_motif="publicData/database/motifs-v9-nr.mgi-m0.001-o0.0.tbl",
        cis_motif="publicData/database/mm9-tss-centered-10kb-7species.mc9nr.feather"
  output:"output/ScenicAll/cis_target/regulons.csv"
  threads:20
  conda:pyscenic_env
  shell:
        "pyscenic ctx --mask_dropouts --annotations_fname {input.TF_motif} \
--expression_mtx_fname {input.exp} -o {output} \
 {input.GRN_boost} {input.cis_motif}"


rule prune_modules_with_cis_target_json_all:
    input:
        exp="output/ScenicAll/expressionRawCountFilteredForScenic.tsv",
        GRN_boost="output/ScenicAll/GRNboost/GRNboost.tsv",
        TF_motif="publicData/database/motifs-v9-nr.mgi-m0.001-o0.0.tbl",
        cis_motif="publicData/database/mm9-tss-centered-10kb-7species.mc9nr.feather"
    output:"output/ScenicAll/cis_target/regulons.json"
    threads:20
    conda:pyscenic_env
    shell:
        "pyscenic ctx --mask_dropouts --annotations_fname {input.TF_motif} \
--expression_mtx_fname {input.exp} -o {output} \
 {input.GRN_boost} {input.cis_motif}"
 

rule aucell_TF_seurat_regulon_all:
    input:"output/ScenicAll/expressionRawCountFilteredForScenic.tsv", "output/ScenicAll/cis_target/regulons.csv"
    output:"output/ScenicAll/AUCell/regulons_enrichment.csv"
    threads:20
    conda:pyscenic_env
    shell:
        "pyscenic aucell --seed 2020 {input[0]} {input[1]} -o {output}"


rule get_report:
    input:"report/final_report.Rmd",
          "output/Seurat3_integration/combined.rds",
          "output/Seurat3_integration/biology/markers_res0.5/markers.tsv",
          "output/Seurat3_integration/biology/markers_res0.5/clusters_table.tsv",
          "report/results/tableRecapSeuratClusters.xlsx",
          "report/results/FACSsorting.csv",
          "output/MonocleIntegrationWoPb/monocleWithCounts.rds",
          expand("output/{age}/MonocleIntegration/monocleWithCounts.rds",age=ages),
          "output/ScenicAll/AUCell/regulons_enrichment.csv",
          "output/ScenicAll/cis_target/regulons.json",
    output:"report/final_report.html","report/seurat_report.rds"
    threads: 12
    conda: report_env  
    shell: "Rscript -e 'rmarkdown::render(\"{input[0]}\")'"

#### Reviewing analysis
rule download_MPP4_rodriguez:
  params: config["Rodriguez_data"]["MPP4"]
  output: "publicData/Rodriguez/rawCount/GSM2411667_MPP4.raw_umifm_counts.csv"
  shell: "cd publicData/Rodriguez/rawCount/;wget {params};gunzip GSM2411667_MPP4.raw_umifm_counts.csv.gz"
  
rule prepare_rodriguez_data_allTypes:
    input: "publicData/Rodriguez/rawCount/GSM2411664_LTHSC.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411668_STHSC.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411665_MPP2.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411666_MPP3.raw_umifm_counts.csv",
          "publicData/Rodriguez/rawCount/GSM2411667_MPP4.raw_umifm_counts.csv",
          sig = "output/signatures/publicSignatures.rds",
          rodriguez = ancient("publicData/rodriguez_cluster_signatures.xlsx")
    output: "output/RodriguezPreparationAllTypes/seurat.rds"
    conda: seurat_monocle_env
    params: num_dim = config["seurat3"]["dim"],
        cr = config["seurat3"]["correction"],
        minPropCell = config["seurat3"]["minPropCellExp"],
        res = config["seurat3"]["resolution"]
    shell: "Rscript R_src/prepareRodriguezAllTypesCL.R -i publicData/Rodriguez/rawCount/ -o output/RodriguezPreparationAllTypes -n {params.num_dim} -b {params.cr} -s {input.sig} -k {input.rodriguez} -r {params.res} -z logNorm"

rule seurat3_integration_withRodriguez:
    input: expand("output/{smp}/castle/seuratTrainedCt.rds",smp=samples),
           "output/RodriguezPreparationAllTypes/seurat.rds"
    output: "output/Seurat3_integration_with_Rodriguez/combined.rds",
    conda: seurat_monocle_env
    params: cr = config["seurat_integration_with_Rodriguez"]["correction"],
            num_dim_smp = config["seurat_integration_with_Rodriguez"]["dim"],
            num_dim_int = config["seurat_integration_with_Rodriguez"]["num_dim_int"],
            smpList = config["seurat_integration_with_Rodriguez"]["smp_list"],
            res = config["seurat_integration_with_Rodriguez"]["resolution"]
    threads: 1
    shell: "Rscript R_src/Seurat3_integration.R -i {params.smpList} -n {params.num_dim_smp} -N {params.num_dim_int} -o output/Seurat3_integration_with_Rodriguez -r {params.res} -b {params.cr} -z logNorm"


rule data_matrix_for_hscScore:
    input: "report/seurat_report.rds"
    output: "output/hscScore/dataMatrix.csv"
    conda: report_env
    shell: "Rscript -e 'seurat <- readRDS(\"{input}\"); \
        matrix <- as.matrix(Seurat::GetAssayData(seurat,slot = \"counts\")); \
        write.csv(matrix,file = \"{output}\",quote = F)'"

rule download_data_for_hscScore:
    params: genes = config["hscScore"]["genes"],model = config["hscScore"]["model"]
    output:"publicData/hscScore/hscScore_model.pkl",
           "publicData/hscScore/model_molo_genes.txt"
    shell:"wget {params.model} -O {output[0]};wget {params.genes} -O {output[1]}"


rule hscScore:
    input: "output/hscScore/dataMatrix.csv",
           "publicData/hscScore/hscScore_model.pkl",
           "publicData/hscScore/model_molo_genes.txt"
    output: "output/hscScore/hscScore.csv"
    conda: hscScore_env
    shell: "python py_src/hscScore.py"

rule get_reportDraftReview:
    input:"report/BMC_reviewing.Rmd",
          "output/Seurat3_integration/combined.rds",
          "output/Seurat3_integration/biology/markers_res0.5/markers.tsv",
          "output/Seurat3_integration/biology/markers_res0.5/clusters_table.tsv",
          "report/results/tableRecapSeuratClusters.xlsx",
          "report/results/FACSsorting.csv",
          "output/MonocleIntegrationWoPb/monocleWithCounts.rds",
          expand("output/{age}/MonocleIntegration/monocleWithCounts.rds",age=ages),
          "output/ScenicAll/AUCell/regulons_enrichment.csv",
          "output/ScenicAll/cis_target/regulons.json",
          "output/hscScore/hscScore.csv",
          "output/Seurat3_integration_with_Rodriguez/combined.rds"
    output:"report/BMC_reviewing.docx"
    threads: 24
    conda: report_env
    shell: "Rscript -e 'rmarkdown::render(\"{input[0]}\")'"

rule get_report_review:
    input:"report/final_report_firstReview.Rmd",
          "output/Seurat3_integration/combined.rds",
          "output/Seurat3_integration/biology/markers_res0.5/markers.tsv",
          "output/Seurat3_integration/biology/markers_res0.5/clusters_table.tsv",
          "report/results/tableRecapSeuratClusters.xlsx",
          "report/results/FACSsorting.csv",
          "output/MonocleIntegrationWoPb/monocleWithCounts.rds",
          expand("output/{age}/MonocleIntegration/monocleWithCounts.rds",age=ages),
          "output/ScenicAll/AUCell/regulons_enrichment.csv",
          "output/ScenicAll/cis_target/regulons.json",
          "output/hscScore/hscScore.csv"
    output:"report/final_report_firstReview.html"	
    threads: 12
    conda: report_env  
    shell: "Rscript -e 'rmarkdown::render(\"{input[0]}\")'"



